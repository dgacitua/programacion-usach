# ¡Bienvenid@s!

Este repositorio tiene por finalidad recopilar links, referencias, documentos y
libros sobre los ramos básicos de Programación de Ingeniería Informática en la
Universidad de Santiago de Chile.

Esta repositorio fue creado por Daniel Gacitúa (daniel.gacitua@usach.cl),
cualquier comentario y sugerencia háganla llegar vía correo.

Por ahora los ramos abarcados son:

    Fundamentos de Computación y Programación
    Métodos de Programación
    Paradigmas de Programación
    Análisis de Algoritmos y Estructura de Datos

Revise la (wiki)[https://bitbucket.org/GaciX/programacion-usach/wiki/Home) para
más información.


# Clonar

Para clonar el repositorio siga los siguientes pasos:

- git clone git@bitbucket.org:GaciX/programacion-usach.git
- cd programacion-usach
- git submodules init
- git submodule update


# Organización

Los contenidos de este repositorio están organizados por carpetas con el nombre
de los ramos. Además esos ramos están dentro de una de dos carpetas: malla2001 y
malla2012.

Dentro de algunas de estas carpetas se pueden encontrar archivos README.md que
explican ciertas peculiaridades de esa carpeta como también una lista de las
fuentes desde donde originalmente se obtuvieron los documentos.


# Colaboradores

Las siguientes personas han ayudado a crear esta compilación (en orden
alfabético):

- Daniel Gacitua
- Felipe Garay
=======

# Repositorio Programación USACH

Éste repositorio en git va a contener material útil para los ramos básicos de programación de las carreras de Ingeniería Informática de la Universidad de Santiago de Chile. También contiene información útil para el curso "Fundamentos de Programación y Computación" dictado en todas las carreras de Ingeniería de la USACH.
